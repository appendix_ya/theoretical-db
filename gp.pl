#!/usr/bin/perl

use Class::Struct;
use DBI;


#################################
#       Struct Defenition       #
#################################
struct Enzyme =>{
        name => '$',
        substrate => '$',
        product => '$',
        constraint => '$',
};


#################################
#	DB connection		#
#################################
my $DB_NAME = "unicorn";
my $DB_HOST = "localhost";
my $DB_USER = "root";
my $DB_PASS = "root";

#my $dbh = DBI->connect("dbi:Pg:dbname=$DB_NAME;host=$DB_HOST", $DB_USER, $DB_PASS)
my $dbh = DBI->connect("dbi:mysql:dbname=$DB_NAME;host=$DB_HOST", $DB_USER, $DB_PASS)
          or die "$!\n ERROR: failed to connect to DB.\n";
my $sthS = $dbh->prepare("SELECT id FROM glystr WHERE structure LIKE ? and size = ?");
my $sthIS = $dbh->prepare("INSERT INTO glystr VALUES (DEFAULT, ?, ?)");
my $sthIT = $dbh->prepare("INSERT INTO glytree VALUES (DEFAULT, ?, ?, ?)");
my $sthSids = $dbh->prepare("SELECT structure FROM glystr WHERE size = ?");

my $sthDs = $dbh->prepare("DELETE FROM glystr where id > 0");
my $sthDt = $dbh->prepare("DELETE FROM glytree where num > 0");
my $sthAs = $dbh->prepare("ALTER TABLE glystr AUTO_INCREMENT = 1");
my $sthAt = $dbh->prepare("ALTER TABLE glytree AUTO_INCREMENT = 1");

$sthDs->execute;
$sthDt->execute;
$sthAs->execute;
$sthAt->execute;


#my ($LC,$enzyme_name,$oya,$ko,$time,$max,$dir);
%info_enzyme;

my $LC = "Ma3(Ma6)Mb4GNb4GN;";		#start glycan structure
my $size = 5;
my $max = 15;
#my $max = 18;
#my $max = 30;



#################################
#	  Enzyme List		#
#################################
#my @input_enzymes = ('FUT1','FUT2','ALG10','ALG11I','ALG11II','ALG12','ALG3','ALG6','ALG8','ST3GAL2','ALG9I','ALG9II','B3GNT1','B3GNT7','B3GALT','B4GALNT','B4GALNT1','B4GALTI','B4GALTII','B4GALTIII','ABOI','ABOII','FUTIII','FUTIV','FUT11I','FUT8','GCNT3','GCNT2','MGAT1','MGAT2','MGAT','MGAT3','MGAT4I','MGAT4III','MGAT5I','MGAT5II','MGAT5III','','MGAT5IV','ST6GAL','ST3GAL3','ST3GAL','ST8SIAI','ST8SIAII','ST8SIAIV','ST8SIAIII');
my @input_enzymes = (
'ABOI','ABOII',
'ALG3','ALG6','ALG8','ALG9I','ALG9II','ALG10','ALG11I','ALG11II','ALG12',
'B3GALT','B3GNT1','B3GNT7',
'B4GALTI','B4GALTII','B4GALTIII','B4GALNT',
'FUT1','FUT2','FUT3','FUT4','FUT5','FUT6','FUT8','FUT11',
'GCNT2','GCNT3','GLCAT',
'MGAT1','MGAT2','MGAT3','MGAT4I','MGAT4III','MGAT5I','MGAT5II','MGAT5III','MGAT5IV',
'ST3GAL','ST3GAL2','ST3GAL3',
'ST6GAL',
'ST8SIAI','ST8SIAII','ST8SIAIII','ST8SIAIV');

&read_enzyme;					#read enzyme information from another file



#################################
#     Make Save Directory	#
#################################
#$time = int(rand(time()));
#$dir = "./GP$time";
#mkdir ($dir) || die "failed $!";




#################################
#	      Main		#
#################################
#my $size = &mass($LC);
#print "(1)size; $size\n";

#my $dir1 = "$dir/$size";
#mkdir ($dir1) || die "failed $!";
#my $outfile = $dir1."/candi_0.txt";

$LC = &reorder($LC);

$sthS->execute($LC, $size);
my $ary_ref = $sthS->fetchrow_arrayref;
my $dbcheck = @{$ary_ref}[0];
if($dbcheck eq ""){
   $sthIS->execute($LC, $size);

   $sthS->execute($LC, $size);
   $ary_ref = $sthS->fetchrow_arrayref;
   $dbcheck = @{$ary_ref}[0];
}

my $koSIZE=$size+1;
foreach my $enzyme_name(@input_enzymes){
   chomp $enzyme_name;
#   &judge($enzyme_name, $LC, $outfile);
   my $koAr = &judge($enzyme_name, $LC);
   foreach my $koLC (@$koAr){
      $koLC = &reorder($koLC);

      if($koLC ne ""){
         $sthS->execute($koLC, $koSIZE);
         my $ary_ref = $sthS->fetchrow_arrayref;
         my $kodbcheck = @{$ary_ref}[0];
         if($kodbcheck eq ""){
            $sthIS->execute($koLC, $koSIZE);

            $sthS->execute($koLC, $koSIZE);
            $ary_ref = $sthS->fetchrow_arrayref;
            $kodbcheck = @{$ary_ref}[0];
         }
         $sthIT->execute($kodbcheck, $dbcheck, $enzyme_name);
      }
   }
}

#&mkuqF($dir, $size);
while($koSIZE < $max){
   my $kokoSIZE=$koSIZE+1;
   $sthSids->execute($koSIZE);
   while (my $list_ref= $sthSids->fetchrow_arrayref){
      $LC = @{$list_ref}[0];

      $sthS->execute($LC, $koSIZE);
      my $ary_ref1 = $sthS->fetchrow_arrayref;
      my $dbcheck = @{$ary_ref}[0];
      if($dbcheck eq ""){
         $sthIS->execute($LC, $size);

         $sthS->execute($LC, $koSIZE);
         $ary_ref = $sthS->fetchrow_arrayref;
         $dbcheck = @{$ary_ref}[0];
      }

      foreach my $enzyme_name(@input_enzymes){
         chomp $enzyme_name;
         my $koAr = &judge($enzyme_name, $LC);
   	 foreach my $koLC (@$koAr){
            $koLC = &reorder($koLC);

            if($koLC ne ""){
               $sthS->execute($koLC, $kokoSIZE);
               my $ary_ref = $sthS->fetchrow_arrayref;
               my $kodbcheck = @{$ary_ref}[0];
               if($kodbcheck eq ""){
                  $sthIS->execute($koLC, $kokoSIZE);

                  $sthS->execute($koLC, $kokoSIZE);
                  $ary_ref = $sthS->fetchrow_arrayref;
                  $kodbcheck = @{$ary_ref}[0];
               }
               $sthIT->execute($kodbcheck, $dbcheck, $enzyme_name);
            }
         }
      }
   }
   $koSIZE++;
}

exit;

#while ($size < $max){
#   $size = &pred($dir, $size, \@input_enzymes);
#   $size = &pred($size, \@input_enzymes);
#}

#exit;






#################################
#	   Subroutines		#
#################################
#====================================#
#read enzyme info from new_enzyme.txt#
#====================================#
sub read_enzyme{
	my $enzyme = Enzyme->new();
	my ($enzymeName, $line);

	open(FILE, "Nenzyme_local.txt");

	$line = <FILE>;
	while ($line ne "///\n"){
		chomp($line);
		if($line =~ /^Enzyme\s:\s(\w+)/){
			$enzymeName = $1;
			$enzyme->name($enzymeName);
		}elsif($line =~ /^Substrate\s:\s(.*)/){
			$enzyme->substrate($1);
		}elsif($line =~ /^Product\s:\s(.*)/){
			$enzyme->product($1);
		}elsif($line =~ /^Constraint\s:\s(.*)/){
			$enzyme->constraint($1);
		}elsif($line =~ /\//){
			my @e = ();
			if (defined $info_enzyme{$enzymeName}){
				@e = @{$info_enzyme{$enzymeName}};
			}
			push (@e, $enzyme);
			$info_enzyme{$enzymeName} = [@e];

			$enzyme = Enzyme->new();
		}

		$line = <FILE>;
	}
}




#======================================================#
#Make child directory				       #
#   directory name = size of predicted glycan structure#
#======================================================#
#sub mkuqF{
#   my ($fold, $uqsize) = @_;
#
#   my $candir = $fold."/".$uqsize;
#   my $margeF = $fold."/margeF_".$uqsize.".txt";
#   my $canfile;
#   open(MG, ">$margeF") or die "failed $!";
#
#      opendir my $dh, $candir or die "$candir:$!";
#      while($canfile = readdir $dh){
#         if($canfile=~ /\.{1,2}$/){
#   	    next;
#         }elsif($canfile=~ /candi_/){
#            my $filein = $candir."/".$canfile;
#
#	    my $mgline = `sort -u $filein`; 
#	    print MG $mgline;
#	    $mgline = "";
##   	    `rm -r $filein`;
#         }
#      }
#      closedir $dh;
#   close(MG);
#
##   `rm -r $candir`;
#
#   my $puqline = `sort -u $margeF`; 
#   my $preuqF = $fold."/puqF_".$uqsize.".txt";
#   open(PUQ, ">$preuqF") or die "failed $!";
#      print PUQ $puqline;
#      $puqline = "";
#   close(PUQ);
##   `rm -r $margeF`;
#
#   return;
#}
#




#=========================================#
#Predict the next level's glycan structure#
#=========================================#
#sub pred{
#   my ($pdir, $psize, $enzy) = @_;
#   my ($psize, $enzy) = @_;
#
#   my @enzymes = @{$enzy};
#   my $csize = $psize + 1;
#   my $version = 0;
#   my $lcsize = 0;
#
##   my $cdir = $pdir."/".$csize;
##   mkdir ($cdir) || die "failed $!";
#
##   my $pfile = $pdir."/puqF_".$psize.".txt";
##   open(FILEIN, $pfile);
##   while(<FILEIN>){
#
##   my $dbcheck;
##   $sthS->execute($lcrule);
##   while (my $ary_ref = $sthS->fetchrow_arrayref){
##      $dbcheck = @{$ary_ref}[0];
##   }
#
#
#   while(<FILEIN>){
#      my $LC = $_;
#      chomp ($LC);
#      $lcsize = &mass($LC);
#      if($lcsize != $psize){
#	 print STDERR "There is an uncounted node at: $LC\n";
#	 exit;
#      }
#
##      my $outfile = $cdir."/candi_".$version.".txt";
##      $version += 1;
#
##      foreach my $enzyme_name(@enzymes){
##	 chomp $enzyme_name;
##	 &judge($enzyme_name, $LC, $outfile);
##      }
#
#   }
#   close(FILEIN);
#
##   &mkuqF($pdir, $csize);
#   return $csize;
#}




#===========#
#judge rules#
#===========#
sub judge{
#   my ($e_name, $oya, $out) = @_;
   my ($e_name, $oya) = @_;
   my @koAr;
   my $ko;
   $oya = "(".$oya;
   my @onenzyme = @{$info_enzyme{$e_name}};

   my $p = qr/ \( (?: [^()] | (??{$p}) )* \) /x;
   my $pat = qr/(\w|$p|\)\()*/;
   my $pat2 = qr/((\w|$p))*/;
#   my $pat2 = qr/($p|\)\()*/;
#   my $pat2 = qr/(\w|$p|\)|\()*/;
   foreach $onenzyme(@onenzyme){
      my $ename = $onenzyme->name;
      my $sub = $onenzyme->substrate;
      my $pro = $onenzyme->product;
      my $con = $onenzyme->constraint;
   
      my @rules;

      if($sub =~ /_/){
   	 my ($a,$b) = split(/_/,$sub);
   	 $sub = qr/$a($pat)?$b/;
      }elsif($sub =~ /-/){
   	 my ($a,$b) = split(/-/,$sub);
   	 $sub = qr/$a($pat2)?$b/x;
      }

      while($oya =~ m/$sub/mg){
   	 my $match = $&;
   	 my $left = $`;
   	 my $space = $1;
   	 my $i = 0;
   
   	 if($con eq 'none'){
   	    ($ko = $oya) =~ s/^(\Q$left\E)$sub/$1$pro/;
   	    $ko =~ s/ /$space/;
   	    $ko =~ s/^\(//;
#   	    open(OUT,">>$out") or die "failed $!";
#   	       print OUT "$ko\n";
#   	    close(OUT);
	    push(@koAr, $ko);
   	 }else{
   	    my @rules;
	    if($con =~ /&/){
   	       @rules = split(/\s&\s/,$con);
   	       foreach $rule(@rules){
   	          if(eval($rule)){
   	             $i++;
   	          }
	       }
   	    }else{
   	       @rules = ($con);
   	       if(eval($con)){
   	          $i++;
   	       }
	    }
   
   	    if(@rules == $i && $i != 0){
   	       ($ko = $oya) =~ s/(\Q$left\E)$sub/$1$pro/;
   	       $ko =~ s/ /$space/;
   	       $ko =~ s/^\(//;
#   	       open(OUT,">>$out") or die "failed $!";
#   	          print OUT "$ko\n";
#   	       close(OUT);
	       push(@koAr, $ko);
   	    }
         }
      }	
   }
   return (\@koAr);
}




#=================#
#Count glycan size#
#=================#
#sub mass{
#   my $glycan = shift @_;
#   my $node = 0;
#
#   while($glycan =~ /([A-Z]{1,2})/g){
#      if(($1 eq 'GN')||($1 eq 'AN')||($1 eq 'NN')){
#         $node++;
#      }elsif(($1 eq 'M')||($1 eq 'A')||($1 eq 'G')||($1 eq 'F')||($1 eq 'X')){
#         $node++;
#      }
#   }
#   return $node;
#}





#===================================#
#Find a parent and children relation#
#===================================#
sub reorder{
   my $glycan = shift @_;
   my $gly_p = $glycan;

#   my %rule = ('G',1,'A',2,'GN',3,'AN',4,'M',5,'NN',6,'F',7,'X',8);
   my %rule = ('G',1,'A',2,'GN',3,'AN',4,'M',5,'NN',6,'F',7,'U',8);
   my @tmp;
   my $count = 0;

   $glycan =~ s/\)/ ) /g;
   $glycan =~ s/\(/ ( /g;
   $glycan =~ s/(\d)/$1 /g;
   $glycan =~ s/;/ ;/g;
   $glycan =~ s/\s+/ /g;

   my @lcAr = split(/\s/,$glycan);
   my $brNumOne = 0;
   my %parchi = ();
   my ($root, $lcrule, $parent);

   for(my $i=0; $i<@lcAr; $i++){
      my $brNumTwo = 0;

      if($lcAr[$i]=~ /([A-Z]{1,2})/){
         my $child = $i."-".$lcAr[$i];
         if($lcAr[$i+1]=~ /[A-Z]{1,2}/){
            $parent = $i+1;
            $parent .= "-".$lcAr[$i+1];

            if(exists($parchi{$parent})){
               push(@{$parchi{$parent}}, $child);
            }else{
               @{$parchi{$parent}} = ($child);
            }

         }elsif($lcAr[$i+1]=~ /;/){
            $root = $i."-".$lcAr[$i];
         }elsif($lcAr[$i+1]=~ /\(/){
            $brNumTwo = 1;
            $parent = &GetParent($i,$brNumTwo,\@lcAr);
            if(exists($parchi{$parent})){
               push(@{$parchi{$parent}}, $child);
            }else{
               @{$parchi{$parent}} =  ($child);
            }
         }elsif($lcAr[$i+1]=~ /\)/){
            $brNumTwo = 0;
            $parent = &GetParent($i,$brNumTwo,\@lcAr);
            if(exists($parchi{$parent})){
               push(@{$parchi{$parent}}, $child);
            }else{
               @{$parchi{$parent}} =  ($child);
            }
         }
      }elsif($lcAr[$i]=~/\(/){
         $brNumOne++;
      }elsif($lcAr[$i]=~/\)/){
         $brNumOne--;
      }
   }

   if($brNumOne != 0){
print "$gly_p\n";
      print "Error at counting brackets\n";
   }

   if($root=~/\d+-(.+)$/){
      $lcrule = $1.";";
   }

   $lcrule = &GetLCrule($root, $lcrule, \%parchi);
   return $lcrule;
}




#==================#
#Find a parent node#
#==================#
sub GetParent {
   my ($index, $count, $lc_array) = @_;
   my @lcArray = @{$lc_array};
   for(my $j=$index+2; $j<@lcArray; $j++){
      if ($lcArray[$j]=~ /[A-Z]{1,2}/ && $count == 0){
         my $parent = $j."-".$lcArray[$j];
         return $parent;
      }elsif ($lcArray[$j]=~ /\(/){
         $count++;
      }elsif ($lcArray[$j]=~ /\)/){
         $count--;
      }
   }
   print "Error at GetParent\n";
   exit;
}




#============================#
#Reordering a lc coded glycan#
#============================#
sub GetLCrule{
   my ($parent, $lcrule, $hash_parchi) = @_;
   my %parchi = %{$hash_parchi};

   my @tmpAr = @{$parchi{$parent}};

   if(@tmpAr >  1){
      my ($prOne, $prOneAB, $prOneBr);

      my @sortAr;
      foreach my $children (@tmpAr){
         if ($children =~ /\d+-([A-Z]{1,2})([ab])(\d+)$/){
            $prOne = $rule{$1};
            $prOneAB = $1.$2.$3;
            $prOneBr = $3;
            push(@sortAr, ([$prOneAB, $prOne, $prOneBr,$children]));
         }
      }
      my @newSortAr = sort{$b->[1] <=> $a->[1] || $b->[2] <=> $a->[2]}@sortAr;

      my $sortSize = @newSortAr-1;

      for(my $i=0; $i<$sortSize; $i++){
         $lcrule = $newSortAr[$i][0].")".$lcrule;
         if(exists($parchi{$newSortAr[$i][3]})){
            $lcrule = &GetLCrule($newSortAr[$i][3], $lcrule, $hash_parchi);
         }
         $lcrule = "(".$lcrule;
      }
      $lcrule = $newSortAr[$sortSize][0].$lcrule;
      if(exists($parchi{$newSortAr[$sortSize][3]})){
         $lcrule = &GetLCrule($newSortAr[$sortSize][3], $lcrule, $hash_parchi);
      }
   }elsif(@tmpAr ==  1){
      my $prOneAB;

      if($tmpAr[0]=~ /\d+-([A-Z]{1,2}[ab]\d+)$/){
         $prOneAB = $1;
         $lcrule = $1.$lcrule;
      }

      if(exists($parchi{$tmpAr[0]})){
         $lcrule = &GetLCrule($tmpAr[0], $lcrule, $hash_parchi);
      }
   }
   return $lcrule;
}





